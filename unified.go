package main

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"time"
)

const UNIFIED_API_BASE_URL = "https://api.tfl.gov.uk"

type ArrivalEntry struct {
	Bearing             string    `json:"bearing"`
	CurrentLocation     string    `json:"currentLocation"`
	DestinationName     string    `json:"destinationName"`
	DestinationNaptanId string    `json:"destinationNaptanId"`
	Direction           string    `json:"direction"`
	ExpectedArrival     time.Time `json:"expectedArrival"`
	ID                  string    `json:"id"`
	LineId              string    `json:"lineId"`
	LineName            string    `json:"lineName"`
	ModeName            string    `json:"modeName"`
	NaptanId            string    `json:"naptanId"`
	OperationType       float64   `json:"operationType"`
	PlatformName        string    `json:"platformName"`
	StationName         string    `json:"stationName"`
	TimeToLive          time.Time `json:"timeToLive"`
	TimeToStation       float64   `json:"timeToStation"`
	Timestamp           time.Time `json:"timestamp"`
	// some timestamps in here fail with:
	// parsing time "\"0001-01-01T00:00:00\"" as "\"2006-01-02T15:04:05Z07:00\"": cannot parse "\"" as "Z07:00"
	Timing struct {
		CountdownServerAdjustment string    `json:"countdownServerAdjustment"`
		Insert                    string    `json:"insert"`
		Read                      time.Time `json:"read"`
		Received                  string    `json:"received"`
		Sent                      time.Time `json:"sent"`
		Source                    string    `json:"source"`
	} `json:"timing"`
	Towards   string `json:"towards"`
	VehicleId string `json:"vehicleId"`
}

func FetchArrivals(line string) ([]ArrivalEntry, error) {
	uri := fmt.Sprintf("%s/Line/%s/Arrivals", UNIFIED_API_BASE_URL, line)
	req, err := http.NewRequest("GET", uri, nil)
	if err != nil {
		return nil, fmt.Errorf("building request failed: %w", err)
	}
	req.Header.Set("User-Agent", "intertube-unified/0.1 (intertube@eta.st)")
	res, err := http.DefaultClient.Do(req)
	if err != nil {
		return nil, fmt.Errorf("http get failed: %w", err)
	}
	var ret []ArrivalEntry
	err = json.NewDecoder(res.Body).Decode(&ret)
	if err != nil {
		return nil, fmt.Errorf("decoding []ArrivalEntry: %w", err)
	}
	return ret, nil
}

func main() {
	arrivals, err := FetchArrivals("DLR")
	if err != nil {
		log.Fatalf("failed to fetch arrivals %v", err)
	}
	log.Printf("got %d arrivals", len(arrivals))
}
