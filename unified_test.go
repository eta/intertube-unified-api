package main

import (
	_ "embed"
	"encoding/json"
	"testing"
)

//go:embed corpus/2022-06-10-dlr-arrivals.json
var DLRArrivals []byte

func TestDLRDecode(t *testing.T) {
	var ret []ArrivalEntry
	err := json.Unmarshal(DLRArrivals, &ret)
	if err != nil {
		panic(err)
	}
}
